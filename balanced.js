const _balancedBracket = {
    openBracket : ['(', '{', '['],
    closeBracket : [')', '}', ']'],
    isBalanced : function(string) {
        let stack = []

        for (let i = 0; i < string.length; i++) {
            const char = string[i]

            if(this.isOpeningBracket(char)) { //check if its open bracket
                stack.push(char) //if true, store it to stack
            } else if(this.isClosingBracket(char)) { //check if its close bracket
                if (stack.length === 0) {
                    return 'NO' //return no if stack is already empty
                } else {
                    stack.pop() //if true, pop the stack to indicates its have pair
                }
            }
        }
        return stack.length === 0 ? 'YES' : 'NO' //true if all stack have pair, no if there still stack unpaired
    },
    isOpeningBracket: function(char) {
        return this.openBracket.includes(char)
    },
    isClosingBracket: function(char) {
        return this.closeBracket.includes(char)
    }
}
            
console.log(_balancedBracket.isBalanced("{{aa")); // Output: 'NO'
console.log(_balancedBracket.isBalanced("{(])}")); // Output: 'NO'
console.log(_balancedBracket.isBalanced("({)]")); // Output: 'YES'