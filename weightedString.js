const _weightedString = {
    calculateWeight : (string) => {
        const weights = [];
        let i = 0;
        
        //loop as string length
        while (i < string.length) {
            //check if next string is same char
            let j = i + 1;
            while (j < string.length && string[j] === string[i]) {
                j++;
            }
            //push for every char weight loop ex. bb = b, bb = 2, 4
            for (let k = 1; k <= j - i ; k++) {
                weights.push((string.charCodeAt(i) - 96) * k);
            }
            //skip to new char index
            i = j;
        }
            
        return weights;
    },
    checkQueries : function (string, queries) {
        const weights = this.calculateWeight(string)
        const results = []
        //check if query values is exist in array of weight
        for(const query of queries) {
            (weights.includes(query)) ? results.push('yes') : results.push('no')
        }

        return results
    }
}

const string = 'abbcccd';
const queries = [1, 3, 9, 8];
const output = _weightedString.checkQueries(string, queries);

console.log(output)
  