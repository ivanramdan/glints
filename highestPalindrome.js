const _palindrome = {
    k : null,
    possibilities : [],
    findHighestPalindrome : function(string, k) {
        const n = string.length
        this.k = k

        this.generatePalindrome(string, n);

        //check maxnumber of possibilities
        const maxNumber = Math.max(...this.possibilities);

        if (this.isPalindrome(maxNumber)) {
            return maxNumber;
        } else {
            return -1;
        }
    },
    generatePalindrome : function(string, n, index = 0) {
        if (this.k === 0 || index >= n / 2) {
            return
        }
      
        // get index char and the opposite of it
        const char = string[index];
        const oppositeChar = string[n - index - 1];
      
        // if its not the same, swap both and store the possibilities
        if (char !== oppositeChar) {
            this.possibilities.push(string.substr(0, index) + oppositeChar + string.substr(index + 1))
            this.possibilities.push(string.substr(0, n-index-1) + char  + string.substr(n-index))
            this.k--;
        }
        // recall function for next index
        return this.generatePalindrome(string, n, index + 1);
    },
    isPalindrome : (number) => {
        const string = number.toString()
        const reversed = string.split('').reverse().join('');
        return string === reversed;
    }
}
  
  // Contoh penggunaan
  const string = '3994';
  const k = 1;
  const palindrom = _palindrome.findHighestPalindrome(string, k);
  console.log(palindrom); // Output: '4994'
  